import {Injectable} from "@angular/core";
import {HttpClient, HttpEvent} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "src/environments/environment";

@Injectable()
export class FilesService {

  constructor(private http: HttpClient) {}

  //define function to upload files
  upload(formData: FormData): Observable<HttpEvent<string[]>> {
    return this.http.post<string[]>(`${environment.serverUrl}/file/upload`, formData, {
      reportProgress: true,
      observe: 'events'
    });
  }

  //define function to download files
  download(filename: string): Observable<HttpEvent<Blob>> {
    return this.http.get(`${environment.serverUrl}/file/download/${filename}`, {
      reportProgress: true,
      observe: 'events',
      responseType: 'blob'
    })
  }

  getListUploadedFiles(): Observable<string[]> {
    return this.http.get<string[]>(`${environment.serverUrl}/file/files-list`);
  }
}
