import {Component, OnInit} from '@angular/core';
import {FilesService} from "src/app/files/services/files.service";
import {HttpErrorResponse, HttpEvent, HttpEventType} from "@angular/common/http";

import {saveAs} from 'file-saver';
import {map} from "rxjs/operators";
import {Observable} from "rxjs";

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.scss']
})
export class FilesComponent implements OnInit {
  filenames: string[] = [];
  fileStatus = {status: '', requestType: '', percent: 0}

  constructor(private fileService: FilesService) { }

  ngOnInit(): void {
    this.getAllUploadedFiles();
  }

  onUploadFiles(files: File[]): void {
    const formData = new FormData();
    for(const file of files) {
      formData.append("files", file, file.name);
      this.fileService.upload(formData).subscribe(
        (event) => {
          this.resportProgress(event)
        },
        ((error: HttpErrorResponse) => console.log(error))
      );
    }
  }

  onDownloadFiles(filename: string): void {
      this.fileService.download(filename).subscribe(
        (event) => {
          this.resportProgress(event)
        },
        ((error: HttpErrorResponse) => console.log(error))
      );
  }

  private resportProgress(httpEvent: HttpEvent<string[] | Blob>): void {

    switch (httpEvent.type) {
      case HttpEventType.UploadProgress:
        this.updateStatus(httpEvent.loaded, httpEvent.total!, 'Uploading...');
        break;

      case HttpEventType.DownloadProgress:
        this.updateStatus(httpEvent.loaded, httpEvent.total!, 'Downloading...');
        break;

      case HttpEventType.ResponseHeader:
        console.log('Header returned', httpEvent);
        break;

      case HttpEventType.Response:
        if(httpEvent.body instanceof Array) {
          for(const filename of httpEvent.body) {
            if(this.filenames.length > 0) {
              if(!this.filenames.includes(filename)) {
                this.filenames.unshift(filename);
              }else{
                //this file alredy exicts
              }
            }
          }
        }else {
          saveAs(new File([httpEvent.body],
                                 httpEvent.headers.get('File-Name'),
                          {type: `${httpEvent.headers.get('Content-Type')};charset=utf-8`}));
          //other variant blob
          /*saveAs(new Blob([httpEvent.body!],
                           {type: `${httpEvent.headers.get('Content-Type')};charset=utf-8`}),
                                  httpEvent.headers.get('File-Name'));*/
        }
        this.fileStatus.status = 'done';
        break;
      default:
        console.log(httpEvent)
    }

  }

  private updateStatus(loaded: number, total: number, requestType: string) {
    this.fileStatus.status = 'progress';
    this.fileStatus.requestType = requestType;
    this.fileStatus.percent = Math.round(100 * loaded / total);
  }

  private getAllUploadedFiles(): void {
    this.fileService.getListUploadedFiles().subscribe(
      (request) => {
         this.filenames.unshift(...request);
      },
      ((error: HttpErrorResponse) => console.log(error))
    );
  }
}
