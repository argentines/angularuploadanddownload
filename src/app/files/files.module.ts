import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from "@angular/common/http";
import {FilesService} from "src/app/files/services/files.service";
import {FilesComponent} from './components/files/files.component';


@NgModule({
  declarations: [FilesComponent],
  imports: [CommonModule, HttpClientModule],
  exports: [FilesComponent],
  providers: [FilesService]
})
export class FilesModule { }
