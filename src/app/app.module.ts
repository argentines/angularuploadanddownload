import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FilesModule} from "src/app/files/files.module";

import { AppComponent } from 'src/app/app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FilesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
